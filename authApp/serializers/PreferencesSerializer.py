from rest_framework import serializers
from authApp.models.Preferences import Preferences

class PreferencesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Preferences
        fields = ['id', 'idUserFK_id' , 'nameCategory']

    def create(self, validated_data):
        PreferencesInstance = Preferences.objects.create(**validated_data)
        return PreferencesInstance

    def to_representation(self, obj):
        preferences = Preferences.objects.get(id=obj.id)
        return {
            'enviado ': obj.id,
            'id': preferences.id,
            'idUserFK': preferences.idUserFK_id,
            'nameCategory': preferences.nameCategory,
        }