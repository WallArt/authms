from django.contrib import admin
from .models.Preferences import Preferences
from .models.User import User

admin.site.register(User)
admin.site.register(Preferences)
# Register your models here.
