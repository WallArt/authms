from django.http.response import HttpResponse
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status, views
from authApp.models.Preferences import Preferences
from authApp.serializers.PreferencesSerializer import PreferencesSerializer
#from django.http import JsonResponse

class PreferencesDetailView(generics.RetrieveAPIView):

    queryset = Preferences.objects.all()
    serializer_class = PreferencesSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
        #return HttpResponse(kwargs['id'])

class PreferencesCreateView(views.APIView):

    def post(self, request, *args, **kwargs):

        serializer = PreferencesSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(request.data, status=status.HTTP_201_CREATED)

class PreferencesUpdateView(generics.UpdateAPIView):
    queryset = Preferences.objects.all()
    serializer_class = PreferencesSerializer

    def put(self, request, *args, **kwargs):

        return super().update(request, *args, **kwargs)

class PreferencesDeleteView(generics.DestroyAPIView):
    queryset = Preferences.objects.all()
    serializer_class = PreferencesSerializer

    def delete(self, request, *args, **kwargs):
        try: 

            return super().destroy(request, *args, **kwargs)
        except:

            stringResponse = {'detail':'Bad Request'}
            return Response(stringResponse, status=status.HTTP_400_BAD_REQUEST)