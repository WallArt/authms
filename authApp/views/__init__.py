from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .userUpdateView import UserUpdateView
from .userDeleteView import UserDeleteView
from .PreferencesViews import PreferencesCreateView
from .PreferencesViews import PreferencesDetailView
from .PreferencesViews import PreferencesUpdateView
from .PreferencesViews import PreferencesDeleteView
from .VerifyTokenView import VerifyTokenView