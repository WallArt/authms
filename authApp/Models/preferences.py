from django.db import models
from .User import User

class Preferences(models.Model):
    id = models.AutoField(primary_key=True)
    idUserFK = models.ForeignKey(User, related_name='idUserFK_Preferences',on_delete=models.CASCADE, default=0)
    nameCategory =  models.CharField('nameCategory', max_length = 25, default='Without')
    